#!/bin/bash

wget -O - http://repos.emulab.net/emulab.key | sudo apt-key add -
echo "deb http://repos.emulab.net/powder/ubuntu $(. /etc/os-release ; echo $UBUNTU_CODENAME) main" | sudo tee /etc/apt/sources.list.d/powder.list
sudo apt-get update

sudo DEBIAN_FRONTEND=noninteractive apt-get install -y gnuradio

sudo sysctl -w net.core.rmem_max=24862979
sudo sysctl -w net.core.wmem_max=24862979

sudo ed /etc/sysctl.conf << "EDEND"
a
net.core.rmem_max=24862979
net.core.wmem_max=24862979
.
w
EDEND

#sudo "/usr/lib/uhd/utils/uhd_images_downloader.py -t x310"
